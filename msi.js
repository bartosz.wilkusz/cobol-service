var Cobol = require("cobol");

function runCobol(keyword, callback) {
    Cobol(__dirname + "/cobol/msi.cbl", {
        args: [keyword]
    }, function (err, data) {
        callback(err || data);
    });
}

module.exports = {
    runCobol
};