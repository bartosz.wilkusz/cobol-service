const express = require('express')
const app = express()
const port = 80

const custRead = require('./custRead')
const transfer = require('./transfer')
const msi = require('./msi')

app.get('/', (req, res) => res.send('Hello World!'))

app.get('/msi', (req, res) => {
    if(req.query.keyword.length > 20){
        res.send("Answer is too long!")
    } else {
        msi.runCobol(req.query.keyword, (result) => {
                res.send(result)
        })
    }
})

app.get('/custRead', (req, res) => {
    if(req.query.iban.length > 9){
        res.send("Invalid IBAN!")
    } else {
        custRead.runCobol(req.query.iban, (result) => {
                res.send(result)
        })
    }
})

app.get('/transfer', (req, res) => {
    var params = [req.query.ibansend, req.query.ibanrec, req.query.amount]
    transfer.runCobol(params, (result) => {
        res.send(result)
    })
    
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
