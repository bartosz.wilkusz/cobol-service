var Cobol = require("cobol");

function runCobol(data, callback) {
    Cobol(__dirname + "/cobol/transfer.cbl",{
        args: [data[0], data[1], data[2]]
    }, function (err, data) {
            callback(err || data);
        });
}

module.exports = {
    runCobol
};