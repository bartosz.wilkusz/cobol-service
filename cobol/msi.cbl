       IDENTIFICATION DIVISION.
       PROGRAM-ID. MSI.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 argv                  PIC X(100) value spaces.
       01 solution1             PIC X(100) value 'GAVZRE'.
       01 solution2             PIC X(100) value 'GAVWRR'.
       
       PROCEDURE DIVISION.
       ACCEPT argv FROM argument-value

       EVALUATE TRUE
           WHEN argv IS EQUAL TO solution1
              DISPLAY "Grace Hopper"
           WHEN argv IS EQUAL TO solution2
              DISPLAY "Rentner"
           WHEN OTHER 
              DISPLAY "Mhhhhmmmm, Steak mit Bratkartoffeln. <3 :P"
       END-EVALUATE

       STOP RUN.

