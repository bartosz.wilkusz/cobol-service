       IDENTIFICATION DIVISION.
       PROGRAM-ID. CUSTOMERREAD.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
          SELECT CUSTOMER ASSIGN TO "Customer.dat"
            ORGANIZATION IS LINE SEQUENTIAL.  
          SELECT TRANSFERS ASSIGN TO "Transfers.dat"
            ORGANIZATION IS LINE SEQUENTIAL.           
       
       DATA DIVISION.
       FILE SECTION.
       FD CUSTOMER.
       01 CUSTOMER-FILE.
          05 CUSTOMER-IBAN PIC X(9).
          05 CUSTOMER-FNAME PIC X(10).
          05 CUSTOMER-LNAME PIC X(10).
          05 CUSTOMER-BALANCE PIC 9(4)V9(2).

       FD TRANSFERS.
       01 TRANSFERS-FILE.
          05 SENDER-IBAN PIC X(9).
          05 RECEIVER-FNAME PIC X(9).
          05 AMOUNT-LNAME PIC 9(4)V9(2).
       
       WORKING-STORAGE SECTION.
       01 WS-CUSTOMER.
          05 WS-IBAN PIC X(9).
          05 WS-FN PIC X(10).
          05 WS-LN PIC X(10).
       01 WS-TRANSFER.
          05 WS-IBAN-SEN PIC X(9).
          05 WS-IBAN-REC PIC X(9).
          05 WS-AMOUNT PIC 9(4)V9(2).
       01 WS-EOF PIC A(1). 
       01 I-PARAM PIC X(9).
       01 WS-BALANCE PIC 9(4)V9(2) VALUE 0000.00.
       
       PROCEDURE DIVISION.
       ACCEPT I-PARAM FROM ARGUMENT-VALUE
          OPEN INPUT CUSTOMER.
             PERFORM UNTIL WS-EOF='Y'
             READ CUSTOMER INTO WS-CUSTOMER
                AT END MOVE 'Y' TO WS-EOF
                NOT AT END 
                  EVALUATE TRUE
                      WHEN WS-IBAN IS EQUAL TO I-PARAM
                           DISPLAY "{""iban"": """WS-IBAN""", ""firstnam
      -                    "e"": """WS-FN""", ""lastname"": """WS-LN""",
      -                    " ""balance"":" 
                  END-EVALUATE
             END-READ
             END-PERFORM.
          CLOSE CUSTOMER.

          MOVE 'N' TO WS-EOF

          OPEN INPUT TRANSFERS.
             PERFORM UNTIL WS-EOF='Y'
             READ TRANSFERS INTO WS-TRANSFER
                AT END MOVE 'Y' TO WS-EOF
                NOT AT END 
                  EVALUATE TRUE
                      WHEN WS-IBAN-SEN IS EQUAL TO I-PARAM
                           SUBTRACT WS-AMOUNT FROM WS-BALANCE
                      WHEN WS-IBAN-REC IS EQUAL TO I-PARAM
                           ADD WS-AMOUNT TO WS-BALANCE
                  END-EVALUATE
             END-READ
             END-PERFORM.
          CLOSE TRANSFERS.

          DISPLAY WS-BALANCE"}"
       STOP RUN.

