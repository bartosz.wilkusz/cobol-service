var Cobol = require("cobol");

function runCobol(iban, callback) {
    Cobol(__dirname + "/cobol/customerRead.cbl",{
        args: [iban]
    }, function (err, data) {
            callback(err || data);
        });
}

module.exports = {
    runCobol
};